package Cabeceras;

import java.util.List;
import Entidades.*;

public class CabeceraUsuario<T> {
    
    private GenUsuario usuario;
    private GenCuenta cuenta;
    private List<Sesion> listSesion;
    private List<GenRolUsuario> listRolUsuario;

    public List<GenRolUsuario> getListRolUsuario() {
        return listRolUsuario;
    }

    public void setListRolUsuario(List<GenRolUsuario> listRolUsuario) {
        this.listRolUsuario = listRolUsuario;
    }

    public List<GenCuentaAplicacion> getListCuentaAplicacion() {
        return listCuentaAplicacion;
    }

    public void setListCuentaAplicacion(List<GenCuentaAplicacion> listCuentaAplicacion) {
        this.listCuentaAplicacion = listCuentaAplicacion;
    }
    private List<GenCuentaAplicacion> listCuentaAplicacion;
    private T tipocabecera;
    public CabeceraUsuario()
    {
        tipocabecera=null; 
    }
    public CabeceraUsuario(T tipocabecera)
    {
        this.tipocabecera=tipocabecera; 
    }

    public T getTipocabecera() {
        return tipocabecera;
    }

    public void setTipocabecera(T tipocabecera) {
        this.tipocabecera = tipocabecera;
    }
    public GenUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(GenUsuario usuario) {
        this.usuario = usuario;
    }

    public GenCuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(GenCuenta cuenta) {
        this.cuenta = cuenta;
    }   

    public List<Sesion> getListSesion() {
        return listSesion;
    }

    public void setListSesion(List<Sesion> listSesion) {
        this.listSesion = listSesion;
    }
}
