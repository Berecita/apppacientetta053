
package Cabeceras;

import java.util.List;

import Entidades.DiarioPaciente;

public class CabeceraDiarioPaciente {
    
    private DiarioPaciente diario;
    private List<CabeceraDia> dias;
    
    public DiarioPaciente getDiario() {
        return diario;
    }

    public void setDiario(DiarioPaciente diario) {
        this.diario = diario;
    }

    public List<CabeceraDia> getDias() {
        return dias;
    }

    public void setDias(List<CabeceraDia> dias) {
        this.dias = dias;
    }    
}
