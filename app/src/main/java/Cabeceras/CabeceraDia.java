
package Cabeceras;

import java.util.List;

import Entidades.AccionRealizada;
import Entidades.DiaPaciente;
import Entidades.EstadoAnimo;

public class CabeceraDia {
    private DiaPaciente dia ;
    private List<EstadoAnimo> estadosDia;
    private List<AccionRealizada> accionesEstado;

    public DiaPaciente getDia() {
        return dia;
    }

    public void setDia(DiaPaciente dia) {
        this.dia = dia;
    }

    public List<EstadoAnimo> getEstadosDia() {
        return estadosDia;
    }

    public void setEstadosDia(List<EstadoAnimo> estadosDia) {
        this.estadosDia = estadosDia;
    }

    public List<AccionRealizada> getAccionesEstado() {
        return accionesEstado;
    }

    public void setAccionesEstado(List<AccionRealizada> accionesEstado) {
        this.accionesEstado = accionesEstado;
    }
    
}
