package ConexionServidor;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

import Cabeceras.CabeceraDiarioPaciente;
import Cabeceras.RestCabeceraDiarioPaciente;




public class RestClientGestionPaciente {

    private String base_URL = "http://tt.solutionsmx.com/ws/Usuario/Paciente/Diario/";

    //"http://192.168.0.10:8080/ws/Usuario/Psicologo/";
            //"http://tt.solutionsmx.com/ws/Usuario/Psicologo/";
   // private String local_URL = "http://localhost:8080/ws/Usuario/Psicologo/";

    private RestTemplate restTemplate = new RestTemplate();

    //lista de Estador por mes
    public CabeceraDiarioPaciente ListaEstadosPorMes(String idPaciente,String mes,String anio)
    {
        Map<String, String> map = new HashMap<>();
       // List<CabeceraUsuario<Paciente>> listCabeceraPacientes;
        restTemplate = new RestTemplate();
        try {
            map.put("idPaciente", idPaciente);
            map.put("mes",mes);
            map.put("anio",anio);
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            ResponseEntity<RestCabeceraDiarioPaciente> listEstados =restTemplate.postForEntity(base_URL + "ListaEstadosMes/'{idPaciente}','{mes}','{anio}'", HttpMethod.POST,  RestCabeceraDiarioPaciente.class, map);
            RestCabeceraDiarioPaciente restCabeceraDiarioPaciente = listEstados.getBody();
            return restCabeceraDiarioPaciente.getCabeceraDiarioPaciente();
        } catch (Exception ex) {
            return null;
        }

    }


//Lista de Estados por día (Bien)
    public CabeceraDiarioPaciente ListaEstados(String idPaciente)
    {
        Map<String, String> map = new HashMap<>();
        RestCabeceraDiarioPaciente cabeceraDiarioPaciente=new RestCabeceraDiarioPaciente();
        restTemplate = new RestTemplate();
        try {
            map.put("idPaciente", idPaciente);
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            String url= base_URL+  "ListaTodosEstados/'{idPaciente}'";
            ResponseEntity<RestCabeceraDiarioPaciente> listEstados =restTemplate.postForEntity(url, HttpMethod.POST,  RestCabeceraDiarioPaciente.class, map);
            cabeceraDiarioPaciente= listEstados.getBody();
            return cabeceraDiarioPaciente.getCabeceraDiarioPaciente();//.getCabeceraDiarioPaciente();
        }
        catch (Exception ex) {
            ex.printStackTrace();//return cabeceraDiarioPaciente.getCabeceraDiarioPaciente();
        }
        return cabeceraDiarioPaciente.getCabeceraDiarioPaciente();

    }

   //Lista de estados por Rango
    public CabeceraDiarioPaciente  ListaEstadosPorRango(String idPaciente,String ini,String fin)
    {
        Map<String, String> map = new HashMap<>();
        RestCabeceraDiarioPaciente cabeceraDiarioPaciente=new RestCabeceraDiarioPaciente();
        restTemplate = new RestTemplate();
        try {
            map.put("idPaciente", idPaciente);
            map.put("ini",ini);
            map.put("fin",fin);
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            String url= base_URL+  "ListaEstadosRango/'{idPaciente}','{ini}','{fin}'";
            ResponseEntity<RestCabeceraDiarioPaciente> listEstado1 =restTemplate.postForEntity(url,HttpMethod.POST, RestCabeceraDiarioPaciente.class, map);
            cabeceraDiarioPaciente= listEstado1.getBody();
            if(cabeceraDiarioPaciente!=null)
            {
                return cabeceraDiarioPaciente.getCabeceraDiarioPaciente();
            }
            else
            {
                return null;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;

    }


    public boolean obtenerEstadosPorRango(String idPaciente,String fecha1,String fecha2)
    {
        restTemplate = new RestTemplate();
        boolean fecha = false;
        try
        {
            Map<String, String> map = new HashMap<>();
            map.put("idPaciente",idPaciente);
            map.put("fecha1",fecha1);
            map.put("fecha2",fecha2);
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            String url=base_URL +"enviarFecha/'{idPaciente}','{fecha1}','{fecha2}'";
            ResponseEntity<Boolean> resultado =   restTemplate.postForEntity(url,HttpMethod.POST, Boolean.class, map);
            fecha= resultado.getBody();
            return fecha;
        }catch (Exception ex)
        {
            return fecha;
        }

    }


    //Agregaremos el estado, actividad y comentario a la base de datos.
    public boolean agregarEstado(String idPaciente,String estado,String actividad,String comentario)
    {
        restTemplate = new RestTemplate();
        boolean status = false;
        try {
            Map<String, String> map = new HashMap<>();
            map.put("idPaciente",idPaciente);
            map.put("estado",estado);
            map.put("actividad",actividad);
            map.put("comentario",comentario);
            restTemplate.getMessageConverters(). add(new MappingJackson2HttpMessageConverter());
            restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            String url=base_URL +"AgregarEstado/'{idPaciente}','{estado}','{actividad}','{comentario}'";
            ResponseEntity<Boolean> resultado =   restTemplate.postForEntity(url,HttpMethod.POST, Boolean.class, map);
            status=resultado.getBody();
            return status;
        } catch (Exception ex) {
            return status;
        }
    }
}
