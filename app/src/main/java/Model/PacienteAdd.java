package Model;

import java.io.Serializable;

import Entidades.*;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PacienteAdd implements Serializable{

  
 
    public GenUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(GenUsuario usuario) {
        this.usuario = usuario;
    }

    public Psicologo getPsicologo() {
        return psicologo;
    }

    public void setPsicologo(Psicologo psicologo) {
        this.psicologo = psicologo;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Tutor getTutor() {
        return tutor;
    }

    public void setTutor(Tutor tutor) {
        this.tutor = tutor;
    }

    @JsonProperty("usuario")
    private GenUsuario usuario;
    @JsonProperty("psicologo")
    private Psicologo psicologo;
    @JsonProperty("paciente")
    private Paciente paciente;
    @JsonProperty("tutor")
    private Tutor tutor;


}
