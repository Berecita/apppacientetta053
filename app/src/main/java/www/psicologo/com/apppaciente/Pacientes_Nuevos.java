package www.psicologo.com.apppaciente;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import www.psicologo.com.apppsicologo.R;


public class Pacientes_Nuevos extends Fragment {


    public Pacientes_Nuevos() {
        // Required empty public constructor
    }

    private View vista;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vista=inflater.inflate(R.layout.fragment_pacientes__nuevos, container, false);

        return vista;
    }

}
