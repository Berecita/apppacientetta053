package www.psicologo.com.apppaciente;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import Cabeceras.CabeceraDiarioPaciente;
import Cabeceras.CabeceraUsuario;
import ConexionServidor.RestClientGestionPaciente;
import Entidades.Paciente;
import EntidadesDiarioPaciente.EstadoAnimo;
import www.psicologo.com.apppsicologo.R;

@SuppressLint("ValidFragment")
public class listado_estados extends Fragment{

    private View vista;

    //variables
    private RecyclerView contenedor;
    private CabeceraUsuario<Paciente> cabeceraUsuario;
    private RestClientGestionPaciente conexionServidor;
    private ProgressDialog progressDialog;
   // private List<CabeceraUsuario<Paciente>> listaEstados; //listaPacientes|
   private List<EstadoAnimo> estadoAnimoList;

    public static String fechaEspanol(Date fecha)
    {
        SimpleDateFormat formateador = new SimpleDateFormat("EEEE ',' dd '/' MMMM '/' yyyy", new Locale("ES"));
        return formateador.format(fecha);
    }


    @SuppressLint("ValidFragment")
    public listado_estados(CabeceraUsuario<Paciente> cabeceraUsuario){
        this.cabeceraUsuario = cabeceraUsuario;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vista = inflater.inflate(R.layout.fragment_lista_estados, container, false);
        inicializar();
        return vista;
    }

    //inicializar

    public void inicializar() {
        //Serìa Lista de Usuarios
        conexionServidor=new RestClientGestionPaciente();
        progressDialog=new ProgressDialog( getContext());
        progressDialog.setTitle("Estados.");

        progressDialog.setMessage("Cargando Estados... Favor de Esperar");
        progressDialog.setCancelable(false);



        progressDialog.show();

        new HttpPeticion().execute();



    }


    //<Entrada del doInBackground, Parametros, Salida de doInBackground>
    // private class EjemploAsynTask extends AsyncTask<Void,Integer,Boolean> {
//<Entrada del doInBackground, Parametros, Salida de doInBackground>
    private class HttpPeticion extends AsyncTask<Void, Void, CabeceraDiarioPaciente> {
        RestClientGestionPaciente conexionServidor;
      //  List<CabeceraUsuario<Paciente>> listPacientes=new ArrayList<CabeceraUsuario<Paciente>>();


        @Override
        protected CabeceraDiarioPaciente doInBackground(Void... params) {
            conexionServidor = new RestClientGestionPaciente();
            CabeceraDiarioPaciente cabeceraDiarioPaciente=new CabeceraDiarioPaciente();
            try
            {

                cabeceraDiarioPaciente=conexionServidor.ListaEstados(cabeceraUsuario.getTipocabecera().getIdPaciente());
            }
            catch(Exception e)
            {
                Log.e("Pacientes_Existente", e.getMessage(), e);
            }
            finally {
                return cabeceraDiarioPaciente;
            }

        }
        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);


            /// progressBar.
        }
        protected void onPostExecute(CabeceraDiarioPaciente cabeceraDiarioPaciente) {

            progressDialog.hide();
            if(cabeceraDiarioPaciente!=null)
            {

                contenedor=(RecyclerView)vista.findViewById(R.id.idCabecerausuarioRecyclerView);
                //no presentara variables en lo cuanto al tamaño
                contenedor.setHasFixedSize(true);
                GridLayoutManager layout= new GridLayoutManager(getContext(),1);
                layout.setOrientation(LinearLayoutManager.VERTICAL);
                Pacientes_Existentes_Adaptador adapter=new Pacientes_Existentes_Adaptador(cabeceraUsuario,getContext(),getFragmentManager(),cabeceraDiarioPaciente.getDias());
                //  adapter.notifyDataSetChanged();
                contenedor.setAdapter(adapter);
                contenedor.setLayoutManager(layout);

            }

        }
    }

}
