package www.psicologo.com.apppaciente;

import java.util.List;

import Cabeceras.CabeceraUsuario;
import Entidades.Paciente;

public interface RecyclerViewInterface {
    public void actualizarModelo(List<CabeceraUsuario<Paciente>> listPaciente);
}
