package www.psicologo.com.apppaciente;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import www.psicologo.com.apppsicologo.R;

public class pantalla_inicio extends AppCompatActivity implements View.OnClickListener {

    private ImageView imageView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_inicio);
        imageView=findViewById(R.id.imageMain);
        imageView.setOnClickListener(this);
    }

    public void onClick(View view) {
        int id =view.getId();
        Intent intent;
        switch (id)
        {
            case R.id.imageMain:

                intent=new Intent(this,LoginActivity.class);
                startActivity(intent);
                finish();
                //onDestroy();
                break;


        }


    }
}
