package www.psicologo.com.apppaciente;


import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import www.psicologo.com.apppsicologo.R;

public  class PopMessageGeneric {

    final TextView title;
    final TextView message;
    final Dialog popDialog;
    public PopMessageGeneric(Context context)
    {
        popDialog=new Dialog(context);
        popDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        popDialog.setCancelable(false);
        //popDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popDialog.setContentView(R.layout.activity_pop_message_generic);

        title=(TextView)popDialog.findViewById(R.id.titleMessage);
        message=(TextView) popDialog.findViewById(R.id.MessageGeneric);
        Button btnContinuar=(Button) popDialog.findViewById(R.id.btnContinuar);
        btnContinuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popDialog.hide();
            }
        });

    }
    public void mostrarMensaje(String titulo,String mensaje)
    {
        title.setText(titulo);
        message.setText(mensaje);

    }
    public void mostrar(boolean estado)
    {
        if(estado)popDialog.show();
        else popDialog.hide();

    }

}
