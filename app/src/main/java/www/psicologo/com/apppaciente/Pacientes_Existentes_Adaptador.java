package www.psicologo.com.apppaciente;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import Cabeceras.CabeceraDia;
import Cabeceras.CabeceraUsuario;
import Entidades.Paciente;
import www.psicologo.com.apppsicologo.R;

public class Pacientes_Existentes_Adaptador extends RecyclerView.Adapter<viewHolderPacientes> {

    private List<CabeceraUsuario<Paciente>> listaEstados;
    public FragmentManager fragmentManager;
    //Pop de Mensaje de COnfirmación
    //   private PopMessageConfirmationGeneric popMessageConfirmationGeneric;
    private ProgressDialog progressDialog;
    private Context context;
    private int valor = -1000;
    final PopMessageGeneric popMessageGeneric;
    private int position;
    private CabeceraUsuario cabeceraUsuario;
    public RecyclerViewInterface intefaz;
    private int funcionEjecutada = -1000;


    private List<CabeceraDia> listcabeceraDia;


    public Pacientes_Existentes_Adaptador(CabeceraUsuario cabeceraUsuario, Context context, FragmentManager fragmentManager, List<CabeceraDia> listcabeceraDia) {
        // this.listaEstados=listaEstados;
        this.fragmentManager = fragmentManager;
        this.context = context;
        //   popMessageConfirmationGeneric=new PopMessageConfirmationGeneric(context,this);
        popMessageGeneric = new PopMessageGeneric(context);
        progressDialog = new ProgressDialog(context);
        this.cabeceraUsuario = cabeceraUsuario;
        this.listcabeceraDia = listcabeceraDia;
    }


    @Override// Se hace referencia a la vista fragment_pacientes_existentes_cardview_itempaciente
    // y sus atributos o componentees
    public viewHolderPacientes onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_pacientes_existentes_cardview_itempaciente, parent, false
        );
       // setListcabeceraDia();
        return new viewHolderPacientes(vista, fragmentManager);
    }

    private ViewGroup layout;

    @Override
    public void onBindViewHolder(viewHolderPacientes holder, final int position) {
        this.position = position;
        // holder.fecha.setText(listcabeceraDia.get(position).getDia().getFecha().toString());
        for (int i = 0; i < listcabeceraDia.get(position).getEstadosDia().size(); i++) {
            layout = (ViewGroup) holder.itemView.findViewById(R.id.ContenedorEstadoAccion);
            LayoutInflater inflater = LayoutInflater.from(holder.itemView.getContext());
            LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.cardviewestado, null, false);
            ImageView ImageEstado = linearLayout.findViewById(R.id.ImageEstado);
            TextView fecha = linearLayout.findViewById(R.id.fecha);
            TextView estadoPaciente = linearLayout.findViewById(R.id.estadoPaciente);
            TextView accionPaciente = linearLayout.findViewById(R.id.accionPaciente);
            TextView comentarioPaciente = linearLayout.findViewById(R.id.comentarioPaciente);
            fecha.setText("FECHA: "+listado_estados.fechaEspanol(listcabeceraDia.get(position).getDia().getFecha()));
            if (listcabeceraDia.get(position).getEstadosDia().get(i).getNombre().equals("Tristeza")) {
                estadoPaciente.setText("ESTADO: "+"Tristeza");
                ImageEstado.setImageResource(R.drawable.triste);
                EstadosCount.tristeza += 1;

            }
            if (listcabeceraDia.get(position).getEstadosDia().get(i).getNombre().equals("Temor")) {
                estadoPaciente.setText("ESTADO: "+"Temor");
                ImageEstado.setImageResource(R.drawable.miedo);
                EstadosCount.miedo += 1;

            }
            if (listcabeceraDia.get(position).getEstadosDia().get(i).getNombre().equals("Alegría")) {
                estadoPaciente.setText("ESTADO: "+"Alegría");
                ImageEstado.setImageResource(R.drawable.feliz);
                EstadosCount.alegria += 1;

            }
            if (listcabeceraDia.get(position).getEstadosDia().get(i).getNombre().equals("Enojo")) {
                estadoPaciente.setText("ESTADO: "+"Enojo");
                ImageEstado.setImageResource(R.drawable.enojado);
                EstadosCount.enojo += 1;

            }
            if (listcabeceraDia.get(position).getEstadosDia().get(i).getNombre().equals("Desagrado")) {
                estadoPaciente.setText("ESTADO: "+"Desagrado");
                ImageEstado.setImageResource(R.drawable.desagrado);
                EstadosCount.desagrado += 1;


            }
            accionPaciente.setText("ACTIVIDAD: "+listcabeceraDia.get(position).getAccionesEstado().get(i).getAccion());
            comentarioPaciente.setText("COMENTARIO: "+listcabeceraDia.get(position).getAccionesEstado().get(i).getComentario());

            layout.addView(linearLayout);
          //  notifyDataSetChanged();

        }
      //  notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listcabeceraDia.size();
    }

}
