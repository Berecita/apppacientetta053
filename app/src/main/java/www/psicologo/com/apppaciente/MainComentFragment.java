package www.psicologo.com.apppaciente;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import Cabeceras.CabeceraUsuario;
import ConexionServidor.RestClientGestionPaciente;
import Entidades.Paciente;
import www.psicologo.com.apppsicologo.R;

@SuppressLint("ValidFragment")
public class MainComentFragment extends Fragment implements View.OnClickListener {
    private View vista;
    private TextView pe, es, act;
    private EditText coment;
    private SharedPreferences SharedPrefer;
    //  private String comentario;
    public String text;
    public String estado, Actividad, comentario;
    public ProgressDialog progressDialog;
    private CabeceraUsuario<Paciente> cabeceraPaciente;

    @SuppressLint("ValidFragment")
    public MainComentFragment(CabeceraUsuario<Paciente> cabeceraPaciente)
    {

        this.cabeceraPaciente=cabeceraPaciente;
    }
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_recuperar_contrasena);
        estado = getArguments().getString("Estado", "Nada");
        Actividad = getArguments().getString("Actividad", "Nada");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        vista = inflater.inflate(R.layout.fragment_main_comentario, container, false);
        pe = (TextView) vista.findViewById(R.id.guardar);
        coment = (EditText) vista.findViewById(R.id.comentario);
        //  es = (TextView) vista.findViewById(R.id.EstadoP);
        // act = (TextView) vista.findViewById(R.id.ActP);

        pe.setOnClickListener(this);
        Button btnGuardar = vista.findViewById(R.id.guardar);
        btnGuardar.setOnClickListener(this);
        // coment.setText(estado);
        //  es.setText(estado);
        // act.setText(Actividad);
        return vista;

    }

    public void onClick(View view) {


        FragmentManager fragmentManager = getFragmentManager();

        if (view.getId() == R.id.guardar) {

            progressDialog=new ProgressDialog(getContext());
            progressDialog.setTitle("Status");
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Agregando estado...");
            progressDialog.show();
            comentario=coment.getText().toString();
            new HttpPeticion().execute();

        }

    }

    //Peticion http

    private class HttpPeticion extends AsyncTask<Void, Void,Boolean>  {

        RestClientGestionPaciente conexcionServer=new RestClientGestionPaciente();
        @Override
        protected Boolean doInBackground(Void... voids) {
            boolean status=conexcionServer.agregarEstado(cabeceraPaciente.getTipocabecera().getIdPaciente(),estado,Actividad,comentario);
            return status;
        }



        protected void onPostExecute(Boolean status) {

            progressDialog.hide();
            if(status)
            {
                Toast.makeText(getContext(),"El estado se agregó",Toast.LENGTH_SHORT).show();
                listado_estados_rango Variable = new listado_estados_rango(cabeceraPaciente);
                Bundle parametro = new Bundle();

                //Estado, actividad y comentario a enviar a vista estadísticas
                parametro.putString("Estado", estado);
                parametro.putString("Actividad", Actividad);
                parametro.putString("Comentario", comentario);



                Variable.setArguments(parametro);
                final FragmentTransaction ft = getFragmentManager().beginTransaction();

                // fragmentManager.beginTransaction().replace(R.id.contenedor,new MainActiFragment()).addToBackStack("Menu").commit();
                ft.replace(R.id.contenedor, Variable, "tag");
                ft.addToBackStack("tag");
                ft.commit();
            }
            else
                {
                    Toast.makeText(getContext(),"Intentalo más tarde",Toast.LENGTH_SHORT).show();

                }

        }
    }
}


