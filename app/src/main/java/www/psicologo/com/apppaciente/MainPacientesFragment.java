package www.psicologo.com.apppaciente;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import www.psicologo.com.apppsicologo.R;


public class MainPacientesFragment extends Fragment implements View.OnClickListener{


    public MainPacientesFragment() {
        // Required empty public constructor
    }
    private View v;
    private TextView pe,pn;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_main_pacientes, container, false);
        pe= (TextView) v.findViewById(R.id.buttonPacienteExistente);
        pn=(TextView) v.findViewById(R.id.buttonPacienteNuevo);

        pe.setOnClickListener(this);
        pn.setOnClickListener(this);
        // Inflate the layout for this fragment
        return v;
    }

    @Override
    public void onClick(View view) {

        FragmentManager fragmentManager= getFragmentManager();

        switch (view.getId())
        {
            case R.id.buttonPacienteNuevo:

                fragmentManager.beginTransaction().replace(R.id.contenedor,new Pacientes_Nuevos()).commit();
                      //Toast.makeText(getContext(),"Botón Nuevo",Toast.LENGTH_LONG).show();
 //               View viewById = getActivity().findViewById(R.id.framentmain_pacientes);
                break;
            case R.id.buttonPacienteExistente:

                Toast.makeText(getContext(),"Botón Existente",Toast.LENGTH_LONG).show();
                break;
            default:
                break;
        }



    }
}
