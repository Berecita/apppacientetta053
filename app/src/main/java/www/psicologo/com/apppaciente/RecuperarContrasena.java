package www.psicologo.com.apppaciente;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

import www.psicologo.com.apppsicologo.R;

public class RecuperarContrasena extends AppCompatActivity implements View.OnClickListener{

    private Button btnRecuContra;
    private EditText correo;
    private String email;
    private ProgressDialog progressDialog;
    private TextView iniSesion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperar_contrasena);

        btnRecuContra=(Button)findViewById(R.id.brecuperarcontrasena);
        btnRecuContra.setOnClickListener(this);
        correo=(EditText)findViewById(R.id.rcontrasenaemail);
        progressDialog=new ProgressDialog(RecuperarContrasena.this);
        iniSesion =(TextView)findViewById(R.id.TextViewIniciarSesion);
        iniSesion.setOnClickListener(this);
        email="";


    }


    //<Entrada del doInBackground, Parametros, Salida de doInBackground>
    // private class EjemploAsynTask extends AsyncTask<Void,Integer,Boolean> {
//<Entrada del doInBackground, Parametros, Salida de doInBackground>
    private class HttpRecuperarContra extends AsyncTask<Void,Void,Boolean> {
        int error=1;
        Boolean estado=false;
        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                //     /Sesion/GenerarCodigo/RecuperarContrasena/'{correo}'"
                final String urlInicioSesion = "http://tt.solutionsmx.com/ws/Sesion/GenerarCodigo/RecuperarContrasena/'{correo}'";//{correo}";


                String urlFinal=urlInicioSesion;
                Map<String,String> map=new HashMap<>();
                map.put("correo",email);

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                restTemplate.postForEntity(urlFinal, HttpMethod.POST,Integer.class,map);
                estado=true;
                return estado;

            } catch (Exception e) {
                Log.e("LoginActivity", e.getMessage(), e);
                return estado;
            }

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);

        }


        protected void onPostExecute(Boolean estado) {

            if(estado)
            {

                progressDialog.hide();
                PopMessageGeneric pop = new PopMessageGeneric(RecuperarContrasena.this);
                pop.mostrarMensaje("Aviso",
                        "Se le ha enviado un link a su correo para que pueda restablecer su contraseña.");
                pop.mostrar(true);
                correo.setText("");


            }
            else
            {
                progressDialog.hide();
                Toast.makeText(getBaseContext(), "Favor de checar su conexión a internet.", Toast.LENGTH_LONG).show();

            }



        }
    }
    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.brecuperarcontrasena:

                if(correo.getText()!=null&&!correo.getText().toString().isEmpty())
                {

                    email=correo.getText().toString();
                    progressDialog.setTitle("Envio Link");
                    progressDialog.setMessage("Enviando peticion para recuperar Contrasena .....");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    new HttpRecuperarContra().execute();
                }
                else
                {
                    Toast.makeText(this,"Es necesario que introduzca su correo",Toast.LENGTH_LONG).show();
                }


                break;
            case R.id.TextViewIniciarSesion:
                Intent intent;
                intent=new Intent(this,LoginActivity.class);
                startActivity(intent);
                break;

            default:
                break;

        }

    }
}

