package www.psicologo.com.apppaciente;



import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import Cabeceras.CabeceraUsuario;
import Entidades.GenUsuario;
import Entidades.Paciente;
import www.psicologo.com.apppsicologo.R;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private CabeceraUsuario<Paciente> cabeceraUsuario;
    private Integer sesion;
    private String correo;
    private ProgressDialog progressDialog;
    private Toolbar toolbar;

    private ImageView imagePerfil;
    private TextView namePsicologo, emailPsicologo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.Principal);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        correo = getIntent().getStringExtra("correo");

        //Encabezado de headNavBar
        View view = navigationView.getHeaderView(0);
        imagePerfil = (ImageView) view.findViewById(R.id.psicologoimage);
        namePsicologo = (TextView) view.findViewById(R.id.psicologoname);
        emailPsicologo = (TextView) view.findViewById(R.id.psicologoemail);

        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setTitle("Perfil");
        progressDialog.setMessage("Cargando.... Porfavor espere.");
        progressDialog.setCancelable(false);
      //  progressDialog.show();
        new HttpPeticion().execute(1);

        //inicializaciónsubmenuPacientes();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.Principal);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        if(getSupportFragmentManager().getBackStackEntryCount()==1)
        {

        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.opciones_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        FragmentManager fragmentManager = getSupportFragmentManager();

        if (id == R.id.men_Pacientes) {
            Toast.makeText(this, "Menu Nuevo Estado", Toast.LENGTH_SHORT).show();
            fragmentManager.beginTransaction().replace(R.id.contenedor, new MainEstadosFragment(cabeceraUsuario)).addToBackStack("Menu").commit();
            // Handle the camera action
        } else if (id == R.id.men_Calendario) {
            Toast.makeText(this, "Listado Estados", Toast.LENGTH_SHORT).show();
            fragmentManager.beginTransaction().replace(R.id.contenedor, new listado_estados_rango(cabeceraUsuario)).addToBackStack("Menu").commit();
        } else if (id == R.id.men_Mail) {
            Toast.makeText(this, "Memorama", Toast.LENGTH_SHORT).show();
            fragmentManager.beginTransaction().replace(R.id.contenedor, new InstruccionesMemorama(cabeceraUsuario)).addToBackStack("Menu").commit();
        } else if (id == R.id.men_Chat) {
            Toast.makeText(this,"Rompecabezas",Toast.LENGTH_SHORT).show();
            fragmentManager.beginTransaction().replace(R.id.contenedor,new InstruccionesRompecabezas(cabeceraUsuario)).addToBackStack("Menu").commit();
       /* } else if (id == R.id.men_ConfigurarC) {
            Toast.makeText(this,"Menu Configurar Cuenta",Toast.LENGTH_SHORT).show();
            fragmentManager.beginTransaction().replace(R.id.contenedor,new MainCuentaFrament()).commit();*/
        } else if (id == R.id.men_CerrarS) {
            progressDialog.setTitle("Aviso");
            progressDialog.setMessage("Cerrando Sesión");
            progressDialog.show();
            new HttpPeticion().execute(2);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.Principal);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


public void cargarVista()
{
    toolbar.setTitle(""+cabeceraUsuario.getUsuario().getNombre() +" "+cabeceraUsuario.getUsuario().getApp());
    namePsicologo.setText(cabeceraUsuario.getUsuario().getNombre() +" "+cabeceraUsuario.getUsuario().getApp()+ " "
            +cabeceraUsuario.getUsuario().getApm());
    emailPsicologo.setText(cabeceraUsuario.getUsuario().getCorreo());

 //   imagePerfil = (ImageView) view.findViewById(R.id.psicologoimage);
   // namePsicologo = (TextView) view.findViewById(R.id.psicologoname);
   // emailPsicologo = (TextView) view.findViewById(R.id.psicologoemail);
}

    private class HttpPeticion extends AsyncTask<Integer, Void, Integer> {

        @Override
        protected Integer doInBackground(Integer... params) {
            String url = "";
            Map<String, String> map = new HashMap<>();
            RestTemplate restTemplate = new RestTemplate();

            try {
               // progressDialog.show();
                if (params[0] != null) {
                    switch (params[0]) {
                        //Recuperar Cabecera
                        case 1:
                            //Se valida si Inicio de Sesion Fue correcta para poder inicializar la cabecera
                            url = "http://tt.solutionsmx.com/ws/Sesion/ObtenerCabecera/'{correo}'";
                            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                            map.put("correo", correo);
                            cabeceraUsuario = null;
                            ResponseEntity<CabeceraUsuario> cabeceraSesion = restTemplate.postForEntity(url, HttpMethod.POST, CabeceraUsuario.class, map);
                            cabeceraUsuario = (CabeceraUsuario<Paciente>) cabeceraSesion.getBody();
                            Object prue = cabeceraUsuario.getTipocabecera();
                            LinkedHashMap linkedHashMap = (LinkedHashMap) prue;
                            Paciente paciente = new Paciente();
                            try{
                            if (linkedHashMap.size() == 5) {
                                paciente.setIdPaciente(linkedHashMap.get("idPaciente").toString());
                                paciente.setAlias(linkedHashMap.get("alias").toString());
                                paciente.setGradoEscolar(linkedHashMap.get("gradoEscolar").toString());
                                paciente.setGenUsuario(new GenUsuario());
                                cabeceraUsuario.setTipocabecera(paciente);
                                getSupportFragmentManager().beginTransaction().replace(R.id.contenedor, new MainEstadosFragment(cabeceraUsuario)).addToBackStack("Menu").commit();
                                return 1;
                            }}catch (Exception ex)
                            {
                                return 3;
                            }
                        //Cerrar Sesión
                        case 2:
                            //Sesion/CerrarSesion/'{idCuenta}'
                            sesion = 0;
                            url = "http://tt.solutionsmx.com/ws/Sesion/CerrarSesion/'{idCuenta}'";
                            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                            map.put("idCuenta", cabeceraUsuario.getCuenta().getIdCuenta());
                            ResponseEntity<Integer> resultado = restTemplate.postForEntity(url, HttpMethod.POST, Integer.class, map);
                            if (resultado != null && resultado.getBody() == 1) {
                                cabeceraUsuario = null;
                                sesion = resultado.getBody();
                            }
                            return 2;

                        default:
                            return 1000;

                    }
                }
            } catch (Exception e) {
                Log.e("MainAcytivity", e.getMessage(), e);
                return null;

            }

            return 1000;
        }


        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            progressDialog.show();
            /// progressBar.
        }


        protected void onPostExecute(Integer valor) {

            if (valor != null) {
                switch (valor) {
                    case 1:
                        if (cabeceraUsuario != null) {
                            progressDialog.hide();
                            cargarVista();
                            Toast.makeText(getBaseContext(), "Bienvenid@ ", Toast.LENGTH_SHORT).show();
                        }

                        break;
                    case 2:
                        if (sesion == 1) {
                            progressDialog.hide();
                            Toast.makeText(getBaseContext(), "Cerrar Sesión", Toast.LENGTH_SHORT).show();
                            Intent intent;
                            intent = new Intent(getBaseContext(), LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }

                        break;
                    case 3:
                        progressDialog.hide();
                        Toast.makeText(getBaseContext(), "El usuario no tiene acceso a esta Aplicación", Toast.LENGTH_SHORT).show();
                        Intent intent;
                        intent = new Intent(getBaseContext(), LoginActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                    default:
                        progressDialog.hide();
                        Toast.makeText(getBaseContext(), "Favor de checar su conexión a Internet. ", Toast.LENGTH_LONG).show();
                        break;

                }


            }

        }
    }
}


