package www.psicologo.com.apppaciente;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

import Cabeceras.CabeceraUsuario;
import Entidades.AccionRealizada;
import Entidades.Paciente;
import www.psicologo.com.apppsicologo.R;


@SuppressLint("ValidFragment")
public class MainEstadisticasFragment extends Fragment{

    private View vista;
    private TextView pe,act,comen;
    public String text;
   //  String est;
    public String variable,actividad,comentario;
    private CabeceraUsuario<Paciente>cabeceraPaciente;
    public int alegria,tristeza,enojo,desagrado,miedo;
    private AccionRealizada accionRealizada;

    @SuppressLint("ValidFragment")
    public MainEstadisticasFragment(CabeceraUsuario<Paciente> cabeceraPaciente)
    {
        this.cabeceraPaciente=cabeceraPaciente;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vista = inflater.inflate(R.layout.fragment_main_estadisticas, container, false);

         //grafica
        PieChart pieChart = (PieChart) vista.findViewById(R.id.chart);

        PieData data = new PieData(getXAxisValues(), getDataSet());
        pieChart.setData(data);
        pieChart.setDescription("Mis emociones");
       // chart.animateXY(2000, 2000);
        pieChart.invalidate();
        EstadosCount.reset();
        return vista;
    }

    private PieDataSet getDataSet() {
        ArrayList<Entry> dataSets = null;

        ArrayList<Entry> valueSet1 = new ArrayList<>();
        Entry v1e1 = new BarEntry(EstadosCount.miedo, 0); // Miedo
        valueSet1.add(v1e1);
        Entry v1e2 = new BarEntry(EstadosCount.tristeza, 1); // Tristeza
        valueSet1.add(v1e2);
        Entry v1e3 = new BarEntry(EstadosCount.alegria, 2); // Alegría
        valueSet1.add(v1e3);
        Entry v1e4 = new BarEntry(EstadosCount.enojo, 3); // Enojo
        valueSet1.add(v1e4);
        Entry v1e5 = new BarEntry(EstadosCount.desagrado, 4); // Desagrado
        valueSet1.add(v1e5);

        PieDataSet pieDataSet=new PieDataSet(valueSet1,"");
        pieDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        pieDataSet.setSliceSpace(3f);
        pieDataSet.setSelectionShift(5f);

        return pieDataSet;
    }

    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("MIEDO");
        xAxis.add("TRISTEZA");
        xAxis.add("ALEGRÍA");
        xAxis.add("ENOJO");
        xAxis.add("DESAGRADO");
       // xAxis.add("JUN");
     //   EstadosCount.reset();
        return xAxis;
    }

}
