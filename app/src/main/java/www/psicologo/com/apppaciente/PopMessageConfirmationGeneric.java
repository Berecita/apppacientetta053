package www.psicologo.com.apppaciente;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import www.psicologo.com.apppsicologo.PopDialogConfirmationGenericInterface;
import www.psicologo.com.apppsicologo.R;

public class PopMessageConfirmationGeneric {

    final TextView title;
    final TextView message;
    final Dialog popDialog;
    private Button aceptar;
    private Button cancelar;
    private www.psicologo.com.apppsicologo.PopDialogConfirmationGenericInterface popIntefaz;

    public PopMessageConfirmationGeneric(Context context, final PopDialogConfirmationGenericInterface popIntefaz)
    {
        popDialog=new Dialog(context);
        popDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        popDialog.setCancelable(false);
        //popDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popDialog.setContentView(R.layout.activity_pop_message_confirmation_generic);

        title=(TextView)popDialog.findViewById(R.id.titleMessage);
        message=(TextView) popDialog.findViewById(R.id.MessageGeneric);
        aceptar=(Button)popDialog.findViewById(R.id.btnGenericConfirmar);


        this.popIntefaz=popIntefaz;
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                popDialog.hide();
                popIntefaz.clicAceptar();
            }
        });

        cancelar=(Button)popDialog.findViewById(R.id.btnGenericCancelar);
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popDialog.hide();
                popIntefaz.clicCancelar();
            }
        });
    }
    public void mostrarMensaje(String titulo,String mensaje)
    {
        title.setText(titulo);
        message.setText(mensaje);

    }
    public void mostrar(boolean estado)
    {
        if(estado)popDialog.show();
        else popDialog.hide();
    }

}
