package www.psicologo.com.apppaciente;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import www.psicologo.com.apppsicologo.R;


public class LoginActivity extends AppCompatActivity implements OnClickListener{


    private ScrollView loginContent;
    private EditText email;
    private EditText password;
    private TextView rpassword;
    private Button biniciarsesion;
    private PopMessageGeneric popMensajeGenerico;
    private Context context;
    private String correo;
    private ProgressDialog progressDialog;


    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email=(EditText) findViewById(R.id.inputemail);
        email.setOnClickListener(this);

        password =(EditText) findViewById(R.id.inputpassword);
        password.setOnClickListener(this);

        rpassword =(TextView) findViewById(R.id.rcontrasena);
        rpassword.setOnClickListener(this);

        biniciarsesion =(Button) findViewById(R.id.biniciarsesion);
        biniciarsesion.setOnClickListener(this);

    }



    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.biniciarsesion:


                String stringEmail = email.getText().toString();
                String stringPassword = password.getText().toString();
                if (!stringEmail.isEmpty() && !stringPassword.isEmpty()) {
                    progressDialog = new ProgressDialog(LoginActivity.this);
                    progressDialog.setTitle("Inicio de Sesión");
                    progressDialog.setMessage("Iniciando Sesion ...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    new HttpRequestTask().execute(stringEmail, stringPassword);
                } else {
                    Toast.makeText(this, "Favor de llenar los campos requeridos.", Toast.LENGTH_LONG).show();

                }

                break;
            case R.id.rcontrasena:
                Intent intent;
                intent = new Intent(this, RecuperarContrasena.class);
                startActivity(intent);
                finish();
                break;

        }
    }

    private class HttpRequestTask extends AsyncTask<String, Void ,Integer> {
        @Override
        protected Integer doInBackground(String... params) {
            try {
                final String urlInicioSesion = "http://tt.solutionsmx.com/ws/Sesion/InicioSesion/";//{correo},{contrasena}";
                correo="";
                correo=params[0];//"stilermarco@hotmail.com";
                String contrasena=params[1];//"coco";

                String urlFinal=urlInicioSesion+correo+","+contrasena;

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ResponseEntity<Integer> respuesta= restTemplate.postForEntity(urlFinal, HttpMethod.POST,Integer.class);



                return respuesta.getBody();

            } catch (Exception e) {
                Log.e("LoginActivity", e.getMessage(), e);
                return null;
            }

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);


        }
        @Override
        protected void onPostExecute(Integer respuesta) {

            Intent intent;

            if(respuesta!=null && respuesta==1)
            {
                //PopMessageGeneric pop=new PopMessageGeneric(context);
                //pop.mostrarMensaje("Este es el Titulo del POP","Estamos probando lo nuevo de los pops para divertirnos un rato");
                //pop.mostrar(true);
                progressDialog.hide();
                intent = new Intent(getBaseContext(), MainActivity.class);
                intent.putExtra("correo",correo);
                startActivity(intent);
              //  finish();
                Toast.makeText(getBaseContext(),"Bienvenid@",Toast.LENGTH_LONG).show();

            }
            else if(respuesta==null)
            {

                progressDialog.hide();
                PopMessageGeneric popM=new PopMessageGeneric(LoginActivity.this);
                popM.mostrarMensaje("Avertencia","Favor de checar su conexión a Internet");
                popM.mostrar(true);

                Toast.makeText(getBaseContext(),"Favor de checar su conexión a Internet. ",Toast.LENGTH_LONG).show();


            }
            else
            {
                progressDialog.hide();
                PopMessageGeneric popM=new PopMessageGeneric(LoginActivity.this);
                popM.mostrarMensaje("Avertencia","El correo o contraseña que introdujo son incorrectos." +
                        " Favor de checar e intentar nuevamente.");
                popM.mostrar(true);



            }








        }
    }

}

