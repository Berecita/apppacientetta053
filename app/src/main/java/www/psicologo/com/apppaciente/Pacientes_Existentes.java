package www.psicologo.com.apppaciente;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import Cabeceras.CabeceraUsuario;
import ConexionServidor.RestClientGestionPaciente;
import Entidades.Paciente;
import www.psicologo.com.apppsicologo.R;


@SuppressLint("ValidFragment")
public class Pacientes_Existentes extends Fragment  {
private View vista;
private RecyclerView contenedor;
private CabeceraUsuario cabeceraUsuario;
private RestClientGestionPaciente conexionServidor;
private ProgressDialog progressDialog;
private List<CabeceraUsuario<Paciente>> listaPacientes;
public Pacientes_Existentes(CabeceraUsuario cabeceraUsuario) {
        // Required empty public constructor
    this.cabeceraUsuario=cabeceraUsuario;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         vista=inflater.inflate(R.layout.fragment_pacientes__existentes, container, false);
        inicializar();
        return vista;
    }
    public void inicializar() {
        //Serìa Lista de Usuarios
        //conexionServidor=new RestClientGestionPaciente();
        progressDialog=new ProgressDialog(getContext());
        progressDialog.setTitle("Listado Pacientes.");
        progressDialog.setMessage("Cargando Estados... Favor de Esperar");
        progressDialog.setCancelable(false);
        progressDialog.show();




       // listaPacientes=new ArrayList<CabeceraUsuario<Paciente>>();
      //  new HttpPeticion(conexionServidor).execute();
        //lista.add("Miriam Razo Sánchez");
        //lista.add("Ana Mirmay Navarro Razo");
        //lista.add("Abigail Rivera Flores");
        //lista.add("Fernando Gómez Arana");




    }




    //<Entrada del doInBackground, Parametros, Salida de doInBackground>
    // private class EjemploAsynTask extends AsyncTask<Void,Integer,Boolean> {
//<Entrada del doInBackground, Parametros, Salida de doInBackground>
  /*  private class HttpPeticion extends AsyncTask<Void, Void, List<CabeceraUsuario<Paciente>>> implements RecyclerViewInterface {
        RestClientGestionPaciente conexionServidor;
        List<CabeceraUsuario<Paciente>> listPacientes=new ArrayList<CabeceraUsuario<Paciente>>();
        @Override
        public void actualizarModelo(List<CabeceraUsuario<Paciente>> listPaciente) {

            //GridLayoutManager layout= new GridLayoutManager(getContext(),1);
            //layout.setOrientation(LinearLayoutManager.VERTICAL);
           // Pacientes_Existentes_Adaptador adapter=new Pacientes_Existentes_Adaptador(cabeceraUsuario,getContext(),listaPacientes,getFragmentManager());
           // adapter.setListaPacientes(listaPacientes);
           // contenedor.setAdapter(adapter);
            contenedor.getAdapter().notifyDataSetChanged();
            //contenedor.setLayoutManager(layout);
        }
        public HttpPeticion(RestClientGestionPaciente conexionServidor)
        {
            this.conexionServidor=conexionServidor;
        }
        @Override
        protected List<CabeceraUsuario<Paciente>> doInBackground(Void... params) {

            try
            {
                CabeceraUsuario<Psicologo> cabeceraPsicologo=(CabeceraUsuario<Psicologo>)cabeceraUsuario;
                listPacientes=conexionServidor.obtenerListaPacientes(cabeceraPsicologo.getTipocabecera().getIdPsicologo());
            }
            catch(Exception e)
            {
                Log.e("Pacientes_Existente", e.getMessage(), e);
            }
            finally {
                return listPacientes;
            }

        }
        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            /// progressBar.
        }
        protected void onPostExecute(List<CabeceraUsuario<Paciente>> listaPacientes) {
            if(listPacientes!=null)
            {
                www.psicologo.com.apppsicologo.PopMessageGeneric popMessageGeneric=new www.psicologo.com.apppsicologo.PopMessageGeneric(getContext());
                popMessageGeneric.mostrarMensaje("Pacientes","Pacientes registrados.");
                popMessageGeneric.mostrar(true);

                //Inicializamos todos los botones y eventos de la vista
                //Aquí se inicializa el RecyclerView cpm ñps Tarjet
                contenedor=(RecyclerView)vista.findViewById(R.id.idPaciienteRecyclerView);
                //no presentara variables en lo cuanto al tamaño
                contenedor.setHasFixedSize(true);
                GridLayoutManager layout= new GridLayoutManager(getContext(),1);
                layout.setOrientation(LinearLayoutManager.VERTICAL);
                Pacientes_Existentes_Adaptador adapter=new Pacientes_Existentes_Adaptador(this,cabeceraUsuario,getContext(),listaPacientes,getFragmentManager());
                adapter.notifyDataSetChanged();
                contenedor.setAdapter(adapter);
                contenedor.setLayoutManager(layout);
                progressDialog.hide();
            }
            else
                {
                    progressDialog.hide();
                    PopMessageGeneric popMessageGeneric=new PopMessageGeneric(getContext());
                    popMessageGeneric.mostrarMensaje("Pacientes","Ocurrio Un error intentar nuevamente.");
                    popMessageGeneric.mostrar(true);

                }
        }
    }*/
}
