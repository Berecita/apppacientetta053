package www.psicologo.com.apppaciente;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import Cabeceras.CabeceraUsuario;
import Entidades.Paciente;
import www.psicologo.com.apppsicologo.R;

@SuppressLint("ValidFragment")
public class InstruccionesMemorama extends Fragment implements View.OnClickListener{

    private View vista;
    private ImageButton comenzar;
    private CabeceraUsuario<Paciente> cabeceraPaciente;

    @SuppressLint("ValidFragment")
    public InstruccionesMemorama(CabeceraUsuario<Paciente> cabeceraPaciente)
    {
        this.cabeceraPaciente=cabeceraPaciente;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vista = inflater.inflate(R.layout.instrucciones_memorama, container, false);
        comenzar = (ImageButton) vista.findViewById(R.id.comenzarMemo);
        comenzar.setOnClickListener(this);

        return vista;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.comenzarMemo:
                MainMemoFragment memorama = new MainMemoFragment(cabeceraPaciente);
                final FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.contenedor, memorama, "tag");
                ft.addToBackStack("menu");
                ft.commit();
                break;
            // Toast.makeText(getContext(),Actividad,Toast.LENGTH_LONG).show();
        }
    }
}
