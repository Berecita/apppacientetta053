package www.psicologo.com.apppsicologo;

public interface PopDialogConfirmationGenericInterface {

    public boolean clicAceptar();
    public boolean clicCancelar();

}
