package www.psicologo.com.apppaciente;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import Cabeceras.CabeceraUsuario;
import Entidades.Paciente;
import www.psicologo.com.apppsicologo.R;

@SuppressLint("ValidFragment")
public class MainActiFragment extends Fragment implements View.OnClickListener {


    private View v;
    public String Actividad;
    private ImageButton b1, b2, b3, b4, b5, b6, b7, b8;
    private TextView EstadoP;
    public String variable;
    public CabeceraUsuario<Paciente> cabeceraPaciente;

 @SuppressLint("ValidFragment")
 public MainActiFragment(CabeceraUsuario<Paciente> cabeceraPaciente)
 {
     this.cabeceraPaciente=cabeceraPaciente;
 }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_recuperar_contrasena);
     variable  = getArguments().getString("Estado","Nada");

    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_main_acti, container, false);
      //  EstadoP= (TextView) v.findViewById(R.id.EstadoP);
        b1 = (ImageButton) v.findViewById(R.id.acti1);
        b2 = (ImageButton) v.findViewById(R.id.acti2);
        b3 = (ImageButton) v.findViewById(R.id.acti3);
        b4 = (ImageButton) v.findViewById(R.id.acti4);
        b5 = (ImageButton) v.findViewById(R.id.acti5);
        b6 = (ImageButton) v.findViewById(R.id.acti6);
        b7 = (ImageButton) v.findViewById(R.id.acti7);
        b8 = (ImageButton) v.findViewById(R.id.acti8);


        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
        b5.setOnClickListener(this);
        b6.setOnClickListener(this);
        b7.setOnClickListener(this);
        b8.setOnClickListener(this);

       // EstadoP.setText(variable);
        return v;


    }


    @Override
    public void onClick(View view) {

        FragmentManager fragmentManager = getFragmentManager();
        MainComentFragment Coment = new MainComentFragment(cabeceraPaciente);
        Bundle parametro = new Bundle();

        Coment.setArguments(parametro);
        final FragmentTransaction ft = getFragmentManager().beginTransaction();

        switch (view.getId()) {
            case R.id.acti1:
                Actividad="Amigos";
               // fragmentManager.beginTransaction().replace(R.id.contenedor, new MainComentFragment()).addToBackStack("Estados").commit();


                parametro.putString("Estado",variable);
                parametro.putString("Actividad",Actividad);
                ft.replace(R.id.contenedor, Coment, "tag");
                ft.addToBackStack("tag");
                ft.commit();
                Toast.makeText(getContext(),Actividad,Toast.LENGTH_LONG).show();
                break;

            case R.id.acti2:
                Actividad="Cine";
               // final FragmentTransaction ft = getFragmentManager().beginTransaction();
                parametro.putString("Estado",variable);
                parametro.putString("Actividad",Actividad);
                ft.replace(R.id.contenedor, Coment, "tag");
                ft.addToBackStack("tag");
                ft.commit();
                //fragmentManager.beginTransaction().replace(R.id.contenedor, new MainComentFragment()).addToBackStack("Estados").commit();
                Toast.makeText(getContext(),Actividad,Toast.LENGTH_LONG).show();
                break;

            case R.id.acti3:
                Actividad="Estudio";
                parametro.putString("Estado",variable);
                parametro.putString("Actividad",Actividad);
                ft.replace(R.id.contenedor, Coment, "tag");
                ft.addToBackStack("tag");
                ft.commit();
                //fragmentManager.beginTransaction().replace(R.id.contenedor, new MainComentFragment()).addToBackStack("Estados").commit();
                Toast.makeText(getContext(),Actividad,Toast.LENGTH_LONG).show();
                break;
            case R.id.acti4:
                Actividad="Estudio";
                parametro.putString("Estado",variable);
                parametro.putString("Actividad",Actividad);
                ft.replace(R.id.contenedor, Coment, "tag");
                ft.addToBackStack("tag");
                ft.commit();
               // fragmentManager.beginTransaction().replace(R.id.contenedor, new MainComentFragment()).addToBackStack("Estados").commit();
                Toast.makeText(getContext(),Actividad,Toast.LENGTH_LONG).show();
                break;
            case R.id.acti5:
                Actividad="Familia";
                parametro.putString("Estado",variable);
                parametro.putString("Actividad",Actividad);
                ft.replace(R.id.contenedor, Coment, "tag");
                ft.addToBackStack("tag");
                ft.commit();
              //  fragmentManager.beginTransaction().replace(R.id.contenedor, new MainComentFragment()).addToBackStack("Estados").commit();
                Toast.makeText(getContext(),Actividad,Toast.LENGTH_LONG).show();
                break;
            case R.id.acti6:
                Actividad="Juego";
                parametro.putString("Estado",variable);
                parametro.putString("Actividad",Actividad);
                ft.replace(R.id.contenedor, Coment, "tag");
                ft.addToBackStack("tag");
                ft.commit();
               // fragmentManager.beginTransaction().replace(R.id.contenedor, new MainComentFragment()).addToBackStack("Estados").commit();
                Toast.makeText(getContext(),Actividad,Toast.LENGTH_LONG).show();
                break;
            case R.id.acti7:
                Actividad="Música";
                parametro.putString("Estado",variable);
                parametro.putString("Actividad",Actividad);
                ft.replace(R.id.contenedor, Coment, "tag");
                ft.addToBackStack("tag");
                ft.commit();
                //fragmentManager.beginTransaction().replace(R.id.contenedor, new MainComentFragment()).addToBackStack("Estados").commit();
                Toast.makeText(getContext(),Actividad,Toast.LENGTH_LONG).show();
                break;
            case R.id.acti8:
                Actividad="Viaje";
                parametro.putString("Estado",variable);
                parametro.putString("Actividad",Actividad);
                ft.replace(R.id.contenedor, Coment, "tag");
                ft.addToBackStack("tag");
                ft.commit();
               // fragmentManager.beginTransaction().replace(R.id.contenedor, new MainComentFragment()).addToBackStack("Estados").commit();
                Toast.makeText(getContext(),Actividad,Toast.LENGTH_LONG).show();
                break;
            default:
                break;
        }
    }
}

