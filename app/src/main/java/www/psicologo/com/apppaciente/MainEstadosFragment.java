package www.psicologo.com.apppaciente;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import Cabeceras.CabeceraUsuario;
import Entidades.Paciente;
import www.psicologo.com.apppsicologo.R;


@SuppressLint("ValidFragment")
public class MainEstadosFragment extends Fragment implements View.OnClickListener {

    private View v;
    private TextView pe,pn;
    private RadioButton op1,op2,op3,op4,op5;
    public String Estado;
    private CabeceraUsuario<Paciente>cabeceraPaciente;

    @SuppressLint("ValidFragment")
    public MainEstadosFragment(CabeceraUsuario<Paciente> cabeceraPaciente)
    {
        this.cabeceraPaciente= cabeceraPaciente;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        v = inflater.inflate(R.layout.fragment_main_estados, container, false);
        pe= (TextView) v.findViewById(R.id.bSig);
        op1=(RadioButton) v.findViewById(R.id.op1);
        op2=(RadioButton) v.findViewById(R.id.op2);
        op3=(RadioButton) v.findViewById(R.id.op3);
        op4=(RadioButton) v.findViewById(R.id.op4);
        op5=(RadioButton) v.findViewById(R.id.op5);

        pe.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View view) {

        FragmentManager fragmentManager= getFragmentManager();

        //Pasar dato al siguiente fragment


        switch (view.getId())
        {
            case R.id.bSig:


                if(op1.isChecked())
                {
                    Estado="Temor";
                    Toast.makeText(getContext(),Estado,Toast.LENGTH_LONG).show();
                }
                if(op2.isChecked())
                {
                    Estado="Tristeza";
                    Toast.makeText(getContext(),Estado,Toast.LENGTH_LONG).show();
                }
                if(op3.isChecked())
                {
                    Estado="Alegría";
                    Toast.makeText(getContext(),Estado,Toast.LENGTH_LONG).show();
                }
                if(op4.isChecked())
                {
                    Estado="Enojo";
                    Toast.makeText(getContext(),Estado,Toast.LENGTH_LONG).show();
                }
                if(op5.isChecked())
                {
                    Estado="Desagrado";
                    Toast.makeText(getContext(),Estado,Toast.LENGTH_LONG).show();
                }

                MainActiFragment Actividad = new MainActiFragment(cabeceraPaciente);
                Bundle parametro = new Bundle();
                parametro.putString("Estado",Estado);
                Actividad.setArguments(parametro);
                final FragmentTransaction ft = getFragmentManager().beginTransaction();

               // fragmentManager.beginTransaction().replace(R.id.contenedor,new MainActiFragment()).addToBackStack("Menu").commit();
                ft.replace(R.id.contenedor, Actividad, "tag");
                ft.addToBackStack("tag");
                ft.commit();

            default:
                break;
        }

    }




}
