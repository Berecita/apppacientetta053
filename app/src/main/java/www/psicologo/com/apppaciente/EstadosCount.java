package www.psicologo.com.apppaciente;

public final class EstadosCount {

    public static float alegria;
    public static float tristeza;
    public static float miedo;
    public static float enojo;
    public static float desagrado;

    public static void reset()
    {
        alegria=0;
        tristeza=0;
        miedo=0;
        enojo=0;
        desagrado=0;

    }

}
