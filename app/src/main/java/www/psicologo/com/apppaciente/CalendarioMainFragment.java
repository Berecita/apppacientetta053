package www.psicologo.com.apppaciente;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import www.psicologo.com.apppsicologo.R;


public class CalendarioMainFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match


    public CalendarioMainFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_calendario_main, container, false);
    }

}
