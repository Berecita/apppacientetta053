package www.psicologo.com.apppaciente;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

import Cabeceras.CabeceraUsuario;
import Entidades.AccionRealizada;
import Entidades.Paciente;
import www.psicologo.com.apppsicologo.R;

@SuppressLint("ValidFragment")
public class MainEstadisticasFragmentPrueba extends Fragment {

    private View vista;
    private TextView pe, act, comen;
    public String text;
    //  String est;
    public String fechaIn,fechaFi;
    public String variable, actividad, comentario;
    private CabeceraUsuario<Paciente> cabeceraPaciente;
    public int alegria, tristeza, enojo, desagrado, miedo;
    private AccionRealizada accionRealizada;
    private float[] yData = {EstadosCount.miedo, EstadosCount.tristeza, EstadosCount.alegria, EstadosCount.enojo, EstadosCount.desagrado};
    private final String[] xData = {"MIEDO", "TRISTEZA", "ALEGRÍA", "ENOJO", "DESAGRADO"};
    private PieChart mChart;
    private TextView countEmocion,porcentEmocion,totalEmociones,rango,countEmocionText,porcentEmocionText,totalEmocionesText;
    @SuppressLint("ValidFragment")
    public MainEstadisticasFragmentPrueba(CabeceraUsuario<Paciente> cabeceraPaciente) {
        this.cabeceraPaciente = cabeceraPaciente;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vista = inflater.inflate(R.layout.fragment_main_estadisticas, container, false);
        countEmocion=vista.findViewById(R.id.idEmocionCount);
        porcentEmocion=vista.findViewById(R.id.idEmocionPorcent);
        totalEmociones=vista.findViewById(R.id.idEmocionesTotal);
        countEmocionText=vista.findViewById(R.id.idEmocionCountText);
        porcentEmocionText=vista.findViewById(R.id.idEmocionPorcentText);
        totalEmocionesText=vista.findViewById(R.id.idEmocionesTotalText);
        rango=vista.findViewById(R.id.rango);
        mChart = (PieChart) vista.findViewById(R.id.chart);

        //Fechas
        //String EMAIL = getArguments() != null ? getArguments().getString("email") : "email@email.com";
       fechaIn = getArguments() !=null? getArguments().getString("Fecha Inicio"): "Fecha Inicio";
       fechaFi = getArguments()!=null? getArguments().getString("Fecha Fin"): "Fecha Fin";


        // configure pie chart
        //        super.onCreate(savedInstanceState);
        mChart.setUsePercentValues(true);
        mChart.setDrawCenterText(true);
        mChart.setDescriptionPosition(0  ,0);
        mChart.setDescription("Emociones");

        // enable hole and configure
        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColorTransparent(true);
        mChart.setHoleRadius(7);
        mChart.setTransparentCircleRadius(10);

        // enable rotation of the chart by touch
        mChart.setRotationAngle(0);
        mChart.setRotationEnabled(true);


        // set a chart value selected listener
        mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {


            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

                rango.setText("Rango: "+fechaIn+" al "+fechaFi);
                if (e == null)
                    return;
                float porcentaje=calcularPorcentaje(e.getVal());
                countEmocionText.setText(""+xData[e.getXIndex()]);
                countEmocion.setText(""+yData[e.getXIndex()]);
                porcentEmocionText.setText("Porcentaje");
                porcentEmocion.setText(String.format("%.1f",porcentaje)+ "%");
                totalEmocionesText.setText("TOTAL");
                totalEmociones.setText(""+totalEmociones());

                //Toast.makeText(getContext(), xData[e.getXIndex()] + " = " +yData[e.getXIndex()], Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected() {

            }
        });

        // add data
        addData();

        // customize legends
        Legend l = mChart.getLegend();
        l.setPosition(Legend.LegendPosition.ABOVE_CHART_LEFT);
        l.setXEntrySpace(10);
        l.setYEntrySpace(5);




        return vista;
    }


    private void addData() {
        ArrayList<Entry> yVals1 = new ArrayList<Entry>();


            for (int i = 0; i < yData.length; i++)
                yVals1.add(new Entry(yData[i], i));

        ArrayList<String> xVals = new ArrayList<String>();

        for (int i = 0; i < xData.length; i++)
            xVals.add(xData[i]);

        // create pie data set
        PieDataSet dataSet = new PieDataSet(yVals1, "");
        dataSet.setSliceSpace(3);
        dataSet.setSelectionShift(5);

        // add many colors
        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());
        dataSet.setColors(colors);

        // instantiate pie data object now
        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.GRAY);

        mChart.setData(data);

        // undo all highlights
        mChart.highlightValues(null);

        // update pie chart
        mChart.invalidate();
    }
    private float totalEmociones()
    {
        return  EstadosCount.miedo+EstadosCount.tristeza+EstadosCount.alegria+EstadosCount.desagrado+EstadosCount.enojo;

    }
    private float calcularPorcentaje(float variable)
    {
        return (variable*100)/totalEmociones();

    }
}
