package www.psicologo.com.apppaciente;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import Cabeceras.CabeceraDiarioPaciente;
import Cabeceras.CabeceraUsuario;
import ConexionServidor.RestClientGestionPaciente;
import Entidades.Paciente;
import EntidadesDiarioPaciente.EstadoAnimo;
import www.psicologo.com.apppsicologo.R;

@SuppressLint("ValidFragment")
public class listado_estados_rango extends Fragment implements View.OnClickListener {


    private View vista;

    //Fecha
    private int dia, mes, anio, diaT, mesT, anioT;
    private int dia1,mes1,anio1;
    private Button consulta;
    private EditText FechaInicio,FechaFin;
    private Button btnGrafica;
    //variables
    private RecyclerView contenedor;
    private CabeceraUsuario<Paciente> cabeceraUsuario;
    private RestClientGestionPaciente conexionServidor;
    private ProgressDialog progressDialog;
    // private List<CabeceraUsuario<Paciente>> listaEstados; //listaPacientes|
    private List<EstadoAnimo> estadoAnimoList;

    public static String fechaEspanol(Date fecha) {
        SimpleDateFormat formateador = new SimpleDateFormat("EEEE ',' dd '/' MMMM '/' yyyy", new Locale("ES"));
        return formateador.format(fecha);
    }

    @SuppressLint("ValidFragment")
    public listado_estados_rango(CabeceraUsuario<Paciente> cabeceraUsuario) {
        this.cabeceraUsuario = cabeceraUsuario;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        vista = inflater.inflate(R.layout.fragment_lista_estados_rango, container, false);
        FechaInicio = vista.findViewById(R.id.idfechaIni);
        FechaInicio.setOnClickListener(this);
        FechaFin = vista.findViewById(R.id.idfechaFin);
        FechaFin.setOnClickListener(this);
        consulta = vista.findViewById(R.id.btnConsultar);
        consulta.setOnClickListener(this);
        btnGrafica= vista.findViewById(R.id.btnMostrarGrafica);
        btnGrafica.setOnClickListener(this);
        inicializar(1);
        return vista;
    }


    public void inicializar(Integer entrada) {
        //Serìa Lista de Usuarios
        conexionServidor = new RestClientGestionPaciente();
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Estados.");

        progressDialog.setMessage("Cargando Estados... Favor de Esperar");
        progressDialog.setCancelable(false);


        progressDialog.show();
        new listado_estados_rango.HttpPeticion().execute(entrada);
    }

    public void onClick(View view) {

                switch (view.getId()) {
                    case R.id.idfechaIni:
                        final Calendar c = Calendar.getInstance();
                        dia = c.get(Calendar.DAY_OF_MONTH);
                        mes = c.get(Calendar.MONTH);
                        anio = c.get(Calendar.YEAR);

                        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                                FechaInicio.setText(d + "-" + (m + 1) + "-" + y);

                            }
                        }, anio, mes, dia);
                        datePickerDialog.show();
                        break;
                    case R.id.idfechaFin:
                        final Calendar c1 = Calendar.getInstance();
                        dia1 = c1.get(Calendar.DAY_OF_MONTH);
                        mes1 = c1.get(Calendar.MONTH);
                        anio1 = c1.get(Calendar.YEAR);

                        DatePickerDialog datePickerDialog1 = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                                FechaFin.setText(d + "-" + (m + 1) + "-" + y);

                            }


                        }, anio1, mes1, dia1);
                        datePickerDialog1.show();
                        break;

                    case R.id.btnConsultar:
                        EstadosCount.reset();
                        inicializar(2);
                        break;
                    case R.id.btnMostrarGrafica:

                        /*getFragmentManager().beginTransaction().
                                replace(R.id.contenedor, new MainEstadisticasFragment(cabeceraUsuario), "tag")
                                .addToBackStack("Estados Paciente").commit();*/

                        MainEstadisticasFragmentPrueba Variable = new MainEstadisticasFragmentPrueba(cabeceraUsuario);
                        Bundle parametro = new Bundle();

                        //Estado, actividad y comentario a enviar a vista estadísticas
                        parametro.putString("Fecha Inicio", FechaInicio.getText().toString());
                        parametro.putString("Fecha Fin", FechaFin.getText().toString());

                        Variable.setArguments(parametro);
                        final FragmentTransaction ft = getFragmentManager().beginTransaction();

                        getFragmentManager().beginTransaction().replace(R.id.contenedor,new MainEstadisticasFragmentPrueba(cabeceraUsuario)).addToBackStack("Menu").commit();
                        ft.replace(R.id.contenedor, Variable, "tag");
                        ft.addToBackStack("tag");
                        ft.commit();

                       /* getFragmentManager().beginTransaction().
                                replace(R.id.contenedor, new MainEstadisticasFragmentPrueba(cabeceraUsuario), "tag")
                                .addToBackStack("Estados Paciente").commit();


*/

                        break;
        }



    }

    private class HttpPeticion extends AsyncTask<Integer, Void, CabeceraDiarioPaciente> {
        RestClientGestionPaciente conexionServidor;
        //  List<CabeceraUsuario<Paciente>> listPacientes=new ArrayList<CabeceraUsuario<Paciente>>();


        @Override
        protected CabeceraDiarioPaciente doInBackground(Integer... params) {
            conexionServidor = new RestClientGestionPaciente();
            CabeceraDiarioPaciente cabeceraDiarioPaciente=new CabeceraDiarioPaciente();
            try {
                if (params[0] != null) {
                    switch (params[0]) {
                        case 1://Listar Todos los Estados
                            cabeceraDiarioPaciente=conexionServidor.ListaEstados(cabeceraUsuario.getTipocabecera().getIdPaciente());
                            EstadosCount.reset();
                            break;
                        case 2://Listar Estados Por Rango Fechas
                            cabeceraDiarioPaciente = conexionServidor.ListaEstadosPorRango(cabeceraUsuario.getTipocabecera().getIdPaciente(), FechaInicio.getText().toString(), FechaFin.getText().toString());
                            break;
                    }


                }
            }
            catch(Exception e)
            {
                Log.e("Pacientes_Existente", e.getMessage(), e);
            }
            finally {
                return cabeceraDiarioPaciente;
            }

        }
        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);


            /// progressBar.
        }
        protected void onPostExecute(CabeceraDiarioPaciente cabeceraDiarioPaciente) {

            progressDialog.hide();
            if(cabeceraDiarioPaciente!=null)
            {

                contenedor=(RecyclerView)vista.findViewById(R.id.idCabecerausuarioRecyclerView);
                //no presentara variables en lo cuanto al tamaño
                contenedor.setHasFixedSize(true);
                GridLayoutManager layout= new GridLayoutManager(getContext(),1);
                layout.setOrientation(LinearLayoutManager.VERTICAL);
                Pacientes_Existentes_Adaptador adapter=new Pacientes_Existentes_Adaptador(cabeceraUsuario,getContext(),getFragmentManager(),cabeceraDiarioPaciente.getDias());
                //  adapter.notifyDataSetChanged();
                contenedor.setAdapter(adapter);
                contenedor.setLayoutManager(layout);

            }

        }
    }

}
