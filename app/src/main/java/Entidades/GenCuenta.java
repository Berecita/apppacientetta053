package Entidades;
// Generated 27/05/2018 04:12:08 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * GenCuenta generated by hbm2java
 */
public class GenCuenta  implements java.io.Serializable {


     private String idCuenta;
     private GenUsuario genUsuario;
     private boolean estado;
//     private Set sesions = new HashSet(0);
//     private Set genCodigoReContrasenas = new HashSet(0);
//     private Set genCuentaAplicacions = new HashSet(0);

    public GenCuenta() {
        this.idCuenta=UUID.randomUUID().toString();
    }

	
    public GenCuenta(String idCuenta, GenUsuario genUsuario, boolean estado) {
        this.idCuenta = idCuenta;
        this.genUsuario = genUsuario;
        this.estado = estado;
    }
    public GenCuenta(String idCuenta, GenUsuario genUsuario, boolean estado, Set sesions, Set genCodigoReContrasenas, Set genCuentaAplicacions) {
       this.idCuenta = idCuenta;
       this.genUsuario = genUsuario;
       this.estado = estado;
//       this.sesions = sesions;
//       this.genCodigoReContrasenas = genCodigoReContrasenas;
//       this.genCuentaAplicacions = genCuentaAplicacions;
    }
   
    public String getIdCuenta() {
        return this.idCuenta;
    }
    
    public void setIdCuenta(String idCuenta) {
        this.idCuenta = idCuenta;
    }
    public GenUsuario getGenUsuario() {
        return this.genUsuario;
    }
    
    public void setGenUsuario(GenUsuario genUsuario) {
        this.genUsuario = genUsuario;
    }
    public boolean isEstado() {
        return this.estado;
    }
    
    public void setEstado(boolean estado) {
        this.estado = estado;
    }
//    public Set getSesions() {
//        return this.sesions;
//    }
//    
//    public void setSesions(Set sesions) {
//        this.sesions = sesions;
//    }
//    public Set getGenCodigoReContrasenas() {
//        return this.genCodigoReContrasenas;
//    }
//    
//    public void setGenCodigoReContrasenas(Set genCodigoReContrasenas) {
//        this.genCodigoReContrasenas = genCodigoReContrasenas;
//    }
//    public Set getGenCuentaAplicacions() {
//        return this.genCuentaAplicacions;
//    }
//    
//    public void setGenCuentaAplicacions(Set genCuentaAplicacions) {
//        this.genCuentaAplicacions = genCuentaAplicacions;
//    }




}


