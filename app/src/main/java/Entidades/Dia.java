package Entidades;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
public class Dia  implements java.io.Serializable {


     private String idDia;
     private Agenda agenda;
     private Date fecha;
//     private Set pendienteses = new HashSet(0);

    public Dia() {
        this.idDia=UUID.randomUUID().toString();
    }

	
    public Dia(String idDia, Agenda agenda, Date fecha) {
        this.idDia = idDia;
        this.agenda = agenda;
        this.fecha = fecha;
    }
    public Dia(String idDia, Agenda agenda, Date fecha, Set pendienteses) {
       this.idDia = idDia;
       this.agenda = agenda;
       this.fecha = fecha;
//       this.pendienteses = pendienteses;
    }
   
    public String getIdDia() {
        return this.idDia;
    }
    
    public void setIdDia(String idDia) {
        this.idDia = idDia;
    }
    public Agenda getAgenda() {
        return this.agenda;
    }
    
    public void setAgenda(Agenda agenda) {
        this.agenda = agenda;
    }
    public Date getFecha() {
        return this.fecha;
    }
    
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
//    public Set getPendienteses() {
//        return this.pendienteses;
//    }
//    
//    public void setPendienteses(Set pendienteses) {
//        this.pendienteses = pendienteses;
//    }




}


