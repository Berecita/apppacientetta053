package Entidades;
import java.util.Date;
import java.util.UUID;

public class AnalisisNeuronal  implements java.io.Serializable {


     private String idAnalisisNeuronal;
     private Pregunta pregunta;
     private Double senial1;
     private Double senial2;
     private Double senial3;
     private Double senial4;
     private Date hora;
     private String analisisNeuronalcol;

    public AnalisisNeuronal() {
        this.idAnalisisNeuronal=UUID.randomUUID().toString();
    }

	
    public AnalisisNeuronal(String idAnalisisNeuronal, Pregunta pregunta) {
        this.idAnalisisNeuronal = idAnalisisNeuronal;
        this.pregunta = pregunta;
    }
    public AnalisisNeuronal(String idAnalisisNeuronal, Pregunta pregunta, Double senial1, Double senial2, Double senial3, Double senial4, Date hora, String analisisNeuronalcol) {
       this.idAnalisisNeuronal = idAnalisisNeuronal;
       this.pregunta = pregunta;
       this.senial1 = senial1;
       this.senial2 = senial2;
       this.senial3 = senial3;
       this.senial4 = senial4;
       this.hora = hora;
       this.analisisNeuronalcol = analisisNeuronalcol;
    }
   
    public String getIdAnalisisNeuronal() {
        return this.idAnalisisNeuronal;
    }
    
    public void setIdAnalisisNeuronal(String idAnalisisNeuronal) {
        this.idAnalisisNeuronal = idAnalisisNeuronal;
    }
    public Pregunta getPregunta() {
        return this.pregunta;
    }
    
    public void setPregunta(Pregunta pregunta) {
        this.pregunta = pregunta;
    }
    public Double getSenial1() {
        return this.senial1;
    }
    
    public void setSenial1(Double senial1) {
        this.senial1 = senial1;
    }
    public Double getSenial2() {
        return this.senial2;
    }
    
    public void setSenial2(Double senial2) {
        this.senial2 = senial2;
    }
    public Double getSenial3() {
        return this.senial3;
    }
    
    public void setSenial3(Double senial3) {
        this.senial3 = senial3;
    }
    public Double getSenial4() {
        return this.senial4;
    }
    
    public void setSenial4(Double senial4) {
        this.senial4 = senial4;
    }
    public Date getHora() {
        return this.hora;
    }
    
    public void setHora(Date hora) {
        this.hora = hora;
    }
    public String getAnalisisNeuronalcol() {
        return this.analisisNeuronalcol;
    }
    
    public void setAnalisisNeuronalcol(String analisisNeuronalcol) {
        this.analisisNeuronalcol = analisisNeuronalcol;
    }




}


