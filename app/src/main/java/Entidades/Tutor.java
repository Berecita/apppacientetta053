package Entidades;
// Generated 27/05/2018 04:12:08 PM by Hibernate Tools 4.3.1

import java.util.UUID;

public class Tutor  implements java.io.Serializable {


     private String idTutor;
     private Paciente paciente;
     private String nombre;
     private String apellidoP;
     private String apellidoM;
     private String correo;
     private String tel;
     private String cel;

    public Tutor() {
        this.idTutor=UUID.randomUUID().toString();
    }

    public Tutor(String idTutor, Paciente paciente, String nombre, String apellidoP, String apellidoM, String correo, String tel, String cel) {
       this.idTutor = idTutor;
       this.paciente = paciente;
       this.nombre = nombre;
       this.apellidoP = apellidoP;
       this.apellidoM = apellidoM;
       this.correo = correo;
       this.tel = tel;
       this.cel = cel;
    }
   
    public String getIdTutor() {
        return this.idTutor;
    }
    
    public void setIdTutor(String idTutor) {
        this.idTutor = idTutor;
    }
    public Paciente getPaciente() {
        return this.paciente;
    }
    
    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellidoP() {
        return this.apellidoP;
    }
    
    public void setApellidoP(String apellidoP) {
        this.apellidoP = apellidoP;
    }
    public String getApellidoM() {
        return this.apellidoM;
    }
    
    public void setApellidoM(String apellidoM) {
        this.apellidoM = apellidoM;
    }
    public String getCorreo() {
        return this.correo;
    }
    
    public void setCorreo(String correo) {
        this.correo = correo;
    }
    public String getTel() {
        return this.tel;
    }
    
    public void setTel(String tel) {
        this.tel = tel;
    }
    public String getCel() {
        return this.cel;
    }
    
    public void setCel(String cel) {
        this.cel = cel;
    }




}


