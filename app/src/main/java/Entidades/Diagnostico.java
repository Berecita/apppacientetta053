package Entidades;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Diagnostico  implements java.io.Serializable {


     private String idDiagnostico;
     private String resumenDiagnostico;
     private String diagnostico;
     //private Set historials = new HashSet(0);

    public Diagnostico() {
    this.idDiagnostico=UUID.randomUUID().toString();
    }

	
    public Diagnostico(String idDiagnostico, String resumenDiagnostico, String diagnostico) {
        this.idDiagnostico = idDiagnostico;
        this.resumenDiagnostico = resumenDiagnostico;
        this.diagnostico = diagnostico;
    }
    public Diagnostico(String idDiagnostico, String resumenDiagnostico, String diagnostico, Set historials) {
       this.idDiagnostico = idDiagnostico;
       this.resumenDiagnostico = resumenDiagnostico;
       this.diagnostico = diagnostico;
       //this.historials = historials;
    }
   
    public String getIdDiagnostico() {
        return this.idDiagnostico;
    }
    
    public void setIdDiagnostico(String idDiagnostico) {
        this.idDiagnostico = idDiagnostico;
    }
    public String getResumenDiagnostico() {
        return this.resumenDiagnostico;
    }
    
    public void setResumenDiagnostico(String resumenDiagnostico) {
        this.resumenDiagnostico = resumenDiagnostico;
    }
    public String getDiagnostico() {
        return this.diagnostico;
    }
    
    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }
//    public Set getHistorials() {
//        return this.historials;
//    }
//    
//    public void setHistorials(Set historials) {
//        this.historials = historials;
//    }




}


