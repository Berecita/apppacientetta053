package Entidades;
import java.util.UUID;

public class AccionRealizada  implements java.io.Serializable {


     private String idAccionRealizada;
     private EstadoAnimo estadoAnimo;
     private String accion;
     private String comentario;

    public AccionRealizada() {
        this.idAccionRealizada=UUID.randomUUID().toString();
    }
    
    public AccionRealizada(EstadoAnimo estadoAnimo)
    {
        this.estadoAnimo=estadoAnimo;
        this.idAccionRealizada=UUID.randomUUID().toString();
    }

	
    public AccionRealizada(String idAccionRealizada, EstadoAnimo estadoAnimo, String accion) {
        this.idAccionRealizada = idAccionRealizada;
        this.estadoAnimo = estadoAnimo;
        this.accion = accion;
    }
    public AccionRealizada(String idAccionRealizada, EstadoAnimo estadoAnimo, String accion, String comentario) {
       this.idAccionRealizada = idAccionRealizada;
       this.estadoAnimo = estadoAnimo;
       this.accion = accion;
       this.comentario = comentario;
    }
   
    public String getIdAccionRealizada() {
        return this.idAccionRealizada;
    }
    
    public void setIdAccionRealizada(String idAccionRealizada) {
        this.idAccionRealizada = idAccionRealizada;
    }
    public EstadoAnimo getEstadoAnimo() {
        return this.estadoAnimo;
    }
    
    public void setEstadoAnimo(EstadoAnimo estadoAnimo) {
        this.estadoAnimo = estadoAnimo;
    }
    public String getAccion() {
        return this.accion;
    }
    
    public void setAccion(String accion) {
        this.accion = accion;
    }
    public String getComentario() {
        return this.comentario;
    }
    
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }




}


