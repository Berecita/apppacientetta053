package EntidadesDiarioPaciente;
// Generated 26/05/2018 02:08:09 PM by Hibernate Tools 4.3.1

import java.util.UUID;

public class AccionRealizada  implements java.io.Serializable {


     private AccionRealizadaId id;
     private EstadoAnimo estadoAnimo;
     private String accion;
     private String comentario;
     
     public AccionRealizada(){
         
     }
    
    public AccionRealizada(String idEstado) {
       id=new AccionRealizadaId();
        id.setIdAccionRealizada(UUID.randomUUID().toString());
        id.setIdEstadoAnimofk(idEstado);
        
    }
    //this.idActividad=UUID.randomUUID().toString();
    public AccionRealizada(AccionRealizadaId id, EstadoAnimo estadoAnimo, String accion) {
        this.id = id;
        this.estadoAnimo = estadoAnimo;
        this.accion = accion;
    }
    public AccionRealizada(AccionRealizadaId id, EstadoAnimo estadoAnimo, String accion, String comentario) {
       this.id = id;
       this.estadoAnimo = estadoAnimo;
       this.accion = accion;
       this.comentario = comentario;
    }
   
    public AccionRealizadaId getId() {
        return this.id;
    }
    
    public void setId(AccionRealizadaId id) {
        this.id = id;
    }
    public EstadoAnimo getEstadoAnimo() {
        return this.estadoAnimo;
    }
    
    public void setEstadoAnimo(EstadoAnimo estadoAnimo) {
        this.estadoAnimo = estadoAnimo;
    }
    public String getAccion() {
        return this.accion;
    }
    
    public void setAccion(String accion) {
        this.accion = accion;
    }
    public String getComentario() {
        return this.comentario;
    }
    
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }




}


