package EntidadesDiarioPaciente;
// Generated 26/05/2018 02:08:09 PM by Hibernate Tools 4.3.1

import java.util.UUID;




/**
 * AccionRealizadaId generated by hbm2java
 */
public class AccionRealizadaId  implements java.io.Serializable {


     private String idAccionRealizada;
     private String idEstadoAnimofk;
     
    public AccionRealizadaId() {
        
    }
    

    public AccionRealizadaId(String idAccionRealizada, String idEstadoAnimofk) {
       this.idAccionRealizada = idAccionRealizada;
       this.idEstadoAnimofk = idEstadoAnimofk;
    }
   
    public String getIdAccionRealizada() {
        return this.idAccionRealizada;
    }
    
    public void setIdAccionRealizada(String idAccionRealizada) {
        this.idAccionRealizada = idAccionRealizada;
    }
    public String getIdEstadoAnimofk() {
        return this.idEstadoAnimofk;
    }
    
    public void setIdEstadoAnimofk(String idEstadoAnimofk) {
        this.idEstadoAnimofk = idEstadoAnimofk;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof AccionRealizadaId) ) return false;
		 AccionRealizadaId castOther = ( AccionRealizadaId ) other; 
         
		 return ( (this.getIdAccionRealizada()==castOther.getIdAccionRealizada()) || ( this.getIdAccionRealizada()!=null && castOther.getIdAccionRealizada()!=null && this.getIdAccionRealizada().equals(castOther.getIdAccionRealizada()) ) )
 && ( (this.getIdEstadoAnimofk()==castOther.getIdEstadoAnimofk()) || ( this.getIdEstadoAnimofk()!=null && castOther.getIdEstadoAnimofk()!=null && this.getIdEstadoAnimofk().equals(castOther.getIdEstadoAnimofk()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getIdAccionRealizada() == null ? 0 : this.getIdAccionRealizada().hashCode() );
         result = 37 * result + ( getIdEstadoAnimofk() == null ? 0 : this.getIdEstadoAnimofk().hashCode() );
         return result;
   }   


}


